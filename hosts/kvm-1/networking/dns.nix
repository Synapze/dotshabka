{ pkgs, lib, ... }:

with lib;

with import <dotshabka/data/space.lama-corp> { }; {

  services.unbound = {
    enable = true;
    interfaces = with fsn.srv.kvm-1; [
      "127.0.0.1"
      "::1"
      wg.v4.ip
      wg.v6.ip
      internal.v4.ip
      internal.v6.ip
    ];
    allowedAccess = [ "0.0.0.0/0" "::0/0" ];
    enableRootTrustAnchor = true;

    extraConfig = ''
        statistics-cumulative: yes
        extended-statistics: yes

        tls-cert-bundle: ${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
        # Do not query the following addresses. No DNS queries are sent there.
        # List one address per entry. List classless netblocks with /size,
        # do-not-query-address: 127.0.0.1/8
        # do-not-query-address: ::1
        # the following makes the above default do-not-query-address entries present.
        do-not-query-localhost: yes

        private-address: 10.0.0.0/8
        private-address: 172.16.0.0/12
        private-address: 192.168.0.0/16
        private-address: 169.254.0.0/16
        private-address: fd00::/8
        private-address: fe80::/10
        private-address: ::ffff:0:0/96

        unblock-lan-zones: yes
        insecure-lan-zones: yes


        ## FSN DNS

        private-domain: "fsn.lama-corp.space"
        domain-insecure: "fsn.lama-corp.space"
        local-zone: "fsn.lama-corp.space." static

        local-data: "kvm-1.srv.fsn.lama-corp.space. IN A ${fsn.srv.kvm-1.wg.v4.ip}"
        local-data-ptr: "${fsn.srv.kvm-1.wg.v4.ip} kvm-1.srv.fsn.lama-corp.space"

        local-data: "hub.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.hub.wg.v4.ip}"
        local-data-ptr: "${fsn.vrt.hub.wg.v4.ip} hub.vrt.fsn.lama-corp.space"

        local-data: "acdc-tp14-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.acdc-tp14-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.acdc-tp14-1.internal.v4.ip} acdc-tp14-1.vrt.fsn.lama-corp.space"

        local-data: "ldap-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.ldap-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.ldap-1.internal.v4.ip} ldap-1.vrt.fsn.lama-corp.space"

        local-data: "mail-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.mail-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.mail-1.internal.v4.ip} mail-1.vrt.fsn.lama-corp.space"

        local-data: "minio-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.minio-1.internal.v4.ip}"
        local-data: "tp14.acdc.risson.space.minio-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.minio-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.minio-1.internal.v4.ip} minio-1.vrt.fsn.lama-corp.space"

        local-data: "postgres-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.postgres-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.postgres-1.internal.v4.ip} postgres-1.vrt.fsn.lama-corp.space"

        local-data: "reverse-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.reverse-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.reverse-1.internal.v4.ip} reverse-1.vrt.fsn.lama-corp.space"

        local-data: "web-1.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.web-1.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.web-1.internal.v4.ip} web-1.vrt.fsn.lama-corp.space"

        local-data: "web-2.vrt.fsn.lama-corp.space. IN A ${fsn.vrt.web-2.internal.v4.ip}"
        local-data-ptr: "${fsn.vrt.web-2.internal.v4.ip} web-2.vrt.fsn.lama-corp.space"


        ## Other DNS

        private-domain: "bar.lama-corp.space"
        domain-insecure: "bar.lama-corp.space"
        local-zone: "2.28.172.in-addr.arpa." transparent
        local-zone: "44.168.192.in-addr.arpa." transparent
        local-data: "nas.srv.bar.lama-corp.space. IN A ${bar.srv.nas.wg.v4.ip}"
        local-data-ptr: "${bar.srv.nas.wg.v4.ip} nas.srv.bar.lama-corp.space"

        private-domain: "drn.lama-corp.space"
        domain-insecure: "drn.lama-corp.space"
        local-zone: "102.28.172.in-addr.arpa." transparent
        local-data: "trunck.lap.drn.lama-corp.space. IN A ${drn.lap.trunck.wg.v4.ip}"
        local-data-ptr: "${drn.lap.trunck.wg.v4.ip} trunck.lap.drn.lama-corp.space"

        private-domain: "nbg.lama-corp.space"
        domain-insecure: "nbg.lama-corp.space"
        local-zone: "3.28.172.in-addr.arpa." transparent
        local-data: "giraffe.srv.nbg.lama-corp.space. IN A ${nbg.srv.giraffe.wg.v4.ip}"
        local-data-ptr: "${nbg.srv.giraffe.wg.v4.ip} giraffe.srv.nbg.lama-corp.space"

        private-domain: "rsn.lama-corp.space"
        domain-insecure: "rsn.lama-corp.space"
        local-zone: "101.28.172.in-addr.arpa." transparent
        local-data: "hedgehog.lap.rsn.lama-corp.space. IN A ${rsn.lap.hedgehog.wg.v4.ip}"
        local-data-ptr: "${rsn.lap.hedgehog.wg.v4.ip} hedgehog.lap.rsn.lama-corp.space"

      forward-zone:
        name: "bar.lama-corp.space."
        forward-addr: ${bar.srv.nas.wg.v4.ip}
      forward-zone:
        name: "44.168.192.in-addr.arpa."
        forward-addr: ${bar.srv.nas.wg.v4.ip}

      forward-zone:
        name: "drn.lama-corp.space."
        forward-addr: ${drn.lap.trunck.wg.v4.ip}
      forward-zone:
        name: "102.28.172.in-addr.arpa."
        forward-addr: ${drn.lap.trunck.wg.v4.ip}

      forward-zone:
        name: "nbg.lama-corp.space."
        forward-addr: ${nbg.srv.giraffe.wg.v4.ip}
      forward-zone:
        name: "3.28.172.in-addr.arpa."
        forward-addr: ${nbg.srv.giraffe.wg.v4.ip}

      forward-zone:
        name: "rsn.lama-corp.space."
        forward-addr: ${rsn.lap.hedgehog.wg.v4.ip}
      forward-zone:
        name: "101.28.172.in-addr.arpa."
        forward-addr: ${rsn.lap.hedgehog.wg.v4.ip}


      forward-zone:
        name: "."
        forward-tls-upstream: yes
        forward-addr: 45.90.28.0#61ba3a.dns1.nextdns.io
        forward-addr: 2a07:a8c0::#61ba3a.dns1.nextdns.io
        forward-addr: 45.90.30.0#61ba3a.dns2.nextdns.io
        forward-addr: 2a07:a8c1::#61ba3a.dns2.nextdns.io
        forward-addr: 1.1.1.1@853#cloudflare-dns.com
        forward-addr: 1.0.0.1@853#cloudflare-dns.com

      remote-control:
        control-enable: yes
        control-interface: "127.0.0.1"
        control-interface: "::1"
        control-port: 8953
        control-use-cert: no
    '';
  };
}
