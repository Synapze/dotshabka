{ ... }:

{
  imports = [
    ./gitlab-runners
    ./hercules-ci.nix
    ./libvirt
    ./TheFractalBot.nix
  ];
}
