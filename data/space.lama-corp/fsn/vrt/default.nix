{ ... }:

{
  acdc-tp14-1 = import ./acdc-tp14-1 { };

  ldap-1 = import ./ldap-1 { };

  mail-1 = import ./mail-1 { };

  minio-1 = import ./minio-1 { };

  postgres-1 = import ./postgres-1 { };

  reverse-1 = import ./reverse-1 { };

  web-1 = import ./web-1 { };

  web-2 = import ./web-2 { };

  virt = import ./virt { };

  hub = import ./hub { };

  lewdax = import ./lewdax { };
}
