{ ... }:

{
  internal = {
    mac = "00:01:2e:48:df:6d";
    interface = "enp4s11";
    v4 = {
      ip = "192.168.44.220";
      prefixLength = 24;
      gw = "192.168.44.254";
    };
  };
}
