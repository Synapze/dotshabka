{ ... }:

{
  internal = {
    mac = "44:fe:3b:1b:ed:3e";
    v4 = {
      ip = "192.168.44.241";
      prefixLength = 24;
      gw = "192.168.44.254";
    };
  };
}
